/*
 *  Qubik: An open source 5x5x5 pico-satellite
 *
 *  Copyright (C) 2020, Libre Space Foundation <http://libre.space>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "main.h"
#include "experiment.h"
#include "radio.h"
#include <string.h>
#include <FreeRTOS.h>
#include "queue.h"
#include "watchdog.h"
#include "error.h"
#include "qubik.h"
#include "telecommand.h"
#include "telemetry.h"
#include "amsat-dl.h"

static struct tx_frame tmp_frame;

extern struct qubik hqubik;
extern struct watchdog hwdg;

static uint8_t tc_exp_buffer[OSDLP_MAX_TC_FRAME_SIZE];

static int
parse_experiment_conf(struct tx_frame_metadata *conf, uint16_t *nframes,
                      const uint8_t *b)
{
	if (!conf || !b) {
		return -INVAL_PARAM;
	}

	conf->enable_pa = b[0] & 0x1;

	conf->tx_power = b[1];
	if (conf->tx_power < -10 || conf->tx_power > 15) {
		return -INVAL_PARAM;
	}

	switch (b[2]) {
		case RADIO_MOD_FSK:
		case RADIO_MOD_BPSK:
		case RADIO_MOD_BPSK_RES:
		case RADIO_MOD_QPSK:
		case RADIO_MOD_RILDOS:
			conf->mod = (radio_modulation_t)b[2];
			break;
		default:
			return -INVAL_PARAM;
	}
	switch (b[3]) {
		case RADIO_ENC_RAW:
		case RADIO_ENC_RS:
		case RADIO_ENC_CC_1_2_RS:
		case RADIO_ENC_SSDV:
			conf->enc = (radio_encoding_t)b[3];
			break;
		default:
			return -INVAL_PARAM;
	}

	switch (b[4]) {
		case RADIO_BAUD_600:
		case RADIO_BAUD_1200:
		case RADIO_BAUD_2400:
		case RADIO_BAUD_4800:
		case RADIO_BAUD_9600:
		case RADIO_BAUD_19200:
		case RADIO_BAUD_38400:
		case RADIO_BAUD_125000:
			conf->baud = (radio_baud_t)b[4];
			break;
		default:
			return -INVAL_PARAM;
	}

	/* Do not allow RILDOS with low data rates */
	if (conf->mod == RADIO_MOD_RILDOS && conf->baud != RADIO_BAUD_125000) {
		return -INVAL_PARAM;
	}

	/* Do not allow the RADIO_BAUD_125000 except RILDOS */
	if (conf->mod != RADIO_MOD_RILDOS && conf->baud == RADIO_BAUD_125000) {
		return -INVAL_PARAM;
	}

	uint16_t n = b[5];
	n = (n << 8) | b[6];
	if (n > 1024) {
		return -INVAL_PARAM;
	}
	*nframes = n;
	return NO_ERROR;
}

void
experiment_task()
{
	uint16_t tc_len;
	uint8_t map;
	uint8_t wdgid;
	int ret = watchdog_register(&hwdg, &wdgid, "experiment");

	if (ret != NO_ERROR) {
		Error_Handler();
	}

	/* Infinite loop */
	for (;;) {
		watchdog_reset_subsystem(&hwdg, wdgid);
		osDelay(100);
		hqubik.experiment_req = 0;

		/* Filter TC messages for the experiment only */
		ret = receive_tc(tc_exp_buffer, &tc_len, &map, VCID_EXPERIMENT);
		if (ret) {
			continue;
		}

		/* Parse the experiment parameters */
		uint16_t nframes;
		ret = parse_experiment_conf(&tmp_frame.meta, &nframes, tc_exp_buffer);
		if (ret) {
			continue;
		}

		/* Ask from FSM permission for experiment */
		hqubik.experiment_completed = 0;
		hqubik.experiment_req = 1;
		uint32_t cnt = 0;
		while (hqubik.fsm_state != FSM_EXPERIMENT_MODE) {
			watchdog_reset_subsystem(&hwdg, wdgid);
			osDelay(1000);
			cnt++;
			/* if the experiment could not be satisfied during
			 * an mean orbit duration, cancel it
			 */
			if (cnt > 9 * 60 || hqubik.experiment_stop) {
				hqubik.experiment_req = 0;
				nframes = 0;
				break;
			}
		}

		/* Start the experiment */
        for (uint16_t i = 0; i < amsat_dl_logo_bin_len; i+=SSDV_PKT_SIZE) {
            memcpy(&tmp_frame.pdu, &amsat_dl_logo_bin + i, SSDV_PKT_SIZE);

            /* This may take a while... */
            watchdog_reset_subsystem(&hwdg, wdgid);

            /* FSM can interrupt the experiment */
            if (hqubik.experiment_stop) {
                hqubik.experiment_completed = 1;
                break;
            }
            ret = -1;
            while (ret) {
                ret = transmit_tm(tmp_frame.pdu, SSDV_PKT_SIZE, VCID_REG_TM,
                                  &tmp_frame.meta);
            }
            /* The minimum duration of a frame can be ~26ms */
            osDelay(20);
        }
		hqubik.experiment_completed = 1;
	}
}
